<?php

 namespace Database\Seeders;

use App\Models\Image;
use App\Models\User;
use App\Models\Role_user;
use Illuminate\Database\Seeder;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
   $users = [
            [
                'name'           => 'User',
                'email'          => 'user@user.com',
                'password'       => bcrypt('password'),
                'remember_token' => null, 
              
            ],
            [
                
              
                  'name'         => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => bcrypt('password'),
                'remember_token' => null,
            ],
            
        ];

        User::insert($users);
       
  


    }
}