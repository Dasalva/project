<?php

namespace Database\Seeders;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
          Role::create([
              'name' => "Invitado",
              'description' =>"Sin privilegios"
          ]);
           Role::create([
              'name' => "Admin",
              'description' =>"Todos los privilegios"
          ]);
    }
}