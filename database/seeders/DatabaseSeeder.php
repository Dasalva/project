<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Image;
use App\Models\User;
use Illuminate\Support\Facades\Storage;






class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Storage::deleteDirectory('images');
        Storage::makeDirectory('images');
        
            Image::factory(1)->create([
                'imageable_id'      => 1,
                'imageable_type'    => User::class
            ]);
            Image::factory(1)->create([
                'imageable_id'      => 2,
                'imageable_type'    => User::class
            ]);
  
             $this->call(UserSeeder::class);
             $this->call(RolesSeeder::class);
        
             $this->call(RoleUserSeeder::class);
            


           

    }
}